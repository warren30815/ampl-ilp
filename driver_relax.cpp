#include "ampl/ampl.h"

using namespace std;

// Ref: https://ampl.com/api/latest/cpp/class-structure.html?highlight=dataframe
// Ref: https://ampl.com/api/latest/cpp/classes/variant.html

int main(int argc, char **argv) {
    if (argc < 4) {
        printf("usage: ./driver FILENAME INITIATOR HOP\n");
        exit(0);
    }

    system(("python3 parser.py " + string(argv[1]) + " " + string(argv[2]) + " " + string(argv[3])).c_str());

    ampl::AMPL ampl;
    ampl.setOption("solver", "gurobi_ampl");
    // ampl.eval(gurobi_option_string);

    ampl.read("model_relax.mod");
    argv[1][strlen(argv[1])-4] = '\0'; ampl.readData(string(argv[1]) + ".dat");
    ampl.solve();

    ampl::DataFrame x = ampl.getVariable("x").getValues();
    printf("[");
    for (auto row : x)
        if (row[1].dbl() > 0.5)
            printf(" %s", row[0].toString().c_str());
    printf(" ]: %f\n", ampl.getObjective("total_significance").value());

    return 0;
}
