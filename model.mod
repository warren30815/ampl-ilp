model;

# Initialized from Data File
set V; # vertex
set E within {V,V}; # edge
param init within {V}; # initiator
param w {V}; # significance
param f {E}; # strength
param UB {V}; # upper bound of local density constraints
param LB {V}; # lower bound of local density constraints
param M; # a very large number for upper bound constraints

# Decision Variables
var x {V} binary;

# Constraints
subject to initiator: x[init] = 1;
subject to upper_bound_local_density_constraints {u in V}:
	sum {(u,v) in E} (f[u,v] * x[v]) <= UB[u] + M * (1 - x[u]);
subject to lower_bound_local_density_constraints {u in V}:
	sum {(u,v) in E} (f[u,v] * x[v]) >= LB[u] * x[u];

# Objective
maximize total_significance: sum {v in V} w[v] * x[v];
