import sys, queue

if len(sys.argv) < 4:
    print('usage: parser.py FILENAME INITIATOR HOP')
    exit(0)

N = 0
list_v = []
list_e = []
adj_list = []

with open(sys.argv[1], 'r') as fr:
    for line in fr.readlines():
        tokens = line.rstrip().split(' ')
        if tokens[0] == '-':
            break
        if tokens[0] != 'c':
            tokens[1] = int(tokens[1])
        if tokens[0] == 'd':
            N = tokens[1]
            list_v = [None] * N
            adj_list = [None] * N
        elif tokens[0] == 'v':
            list_v[tokens[1]] = (tokens[2], tokens[3], tokens[4])
        elif tokens[0] == 'e':
            tokens[2] = int(tokens[2])
            list_e.append((tokens[1], tokens[2], tokens[3]))
            list_e.append((tokens[2], tokens[1], tokens[3]))
            if adj_list[tokens[1]] is None:
                adj_list[tokens[1]] = [tokens[2]]
            else:
                adj_list[tokens[1]].append(tokens[2])
            if adj_list[tokens[2]] is None:
                adj_list[tokens[2]] = [tokens[1]]
            else:
                adj_list[tokens[2]].append(tokens[1])

hop = int(sys.argv[3])
q = queue.Queue()
q.put((int(sys.argv[2]), 0))
visited = [int(sys.argv[2])]
while (not q.empty()):
    root = q.get_nowait()
    dist = root[1]
    if dist >= hop:
        break
    root = root[0]
    for v in adj_list[root]:
        if v not in visited:
            q.put((v, dist + 1))
            visited.append(v)
list_v = [(v, list_v[v]) for v in visited]
list_e = [e for e in list_e if (e[0] in visited) and (e[1] in visited)]

with open(sys.argv[1][:-4] + '.dat', 'w') as fw:
    print('data;', file=fw)
    print('set V :=', end='', file=fw)
    for x in visited:
        print(' ' + str(x), end='', file=fw)
    print(';', file=fw)
    print('param init := ' + sys.argv[2] + ';', file=fw)
    print('param: w :=', file=fw)
    for x in list_v:
        print('  ' + str(x[0]) + ' ' + str(x[1][0]), file=fw)
    print(';', file=fw)
    print('param: LB :=', file=fw)
    for x in list_v:
        print('  ' + str(x[0]) + ' ' + str(x[1][1]), file=fw)
    print(';', file=fw)
    print('param: UB :=', file=fw)
    for x in list_v:
        print('  ' + str(x[0]) + ' ' + str(x[1][2]), file=fw)
    print(';', file=fw)
    print('set E :=', end='', file=fw)
    for x in list_e:
        print(' (' + str(x[0]) + ',' + str(x[1]) + ')', end='', file=fw)
    print(';', file=fw)
    print('param: f :=', file=fw)
    for x in list_e:
        print('  ' + str(x[0]) + ' ' + str(x[1]) + ' ' + x[2], file=fw)
    print(';', file=fw)
    print('param M := ' + str(sum([x[0] for x in list_e])) + ';', file=fw)
